pyferret (7.3-3) UNRELEASED; urgency=medium

  * Drop Java dependencies; threddsBrowser code no longer shipped

 -- Alastair McKinstry <mckinstry@debian.org>  Fri, 02 Mar 2018 15:45:18 +0000

pyferret (7.3-2) unstable; urgency=medium

  * Standards-Version: 4.1.3
  * Point VCS to salsa.debian.org

 -- Alastair McKinstry <mckinstry@debian.org>  Fri, 02 Mar 2018 13:13:14 +0000

pyferret (7.3-1) unstable; urgency=medium

  * New upstream release
  * Standards-Version: 4.1.2; no changes required
  * Fix find in clean target to delete properly. Closes: #876723
  * Add recommends for scipy, pyshp

 -- Alastair McKinstry <mckinstry@debian.org>  Sat, 09 Dec 2017 12:40:50 +0000

pyferret (7.2-1) unstable; urgency=medium

  * Mew upstream release
  * Patch from Simon Quigleyo ensure we are py3-version neutral.
    Closes: #869235
  * Add gbp.conf file
  * __init__.py: Add default values for FER_PALETTE, etc.

 -- Alastair McKinstry <mckinstry@debian.org>  Sat, 22 Jul 2017 16:43:34 +0100

pyferret (7.1.1.beta-1) unstable; urgency=medium

  * New upstream release.
    - remove obsolete py3 patches
    - patch to fixup setup.py for py3 libname
  * Ignore tests for this build 
  * Standards-Version: 4.0.0. No changes required
  * Enable builds with python3.5, py3.6 simultaneously. Closes: #863004

 -- Alastair McKinstry <mckinstry@debian.org>  Sun, 25 Jun 2017 11:05:57 +0100

pyferret (7.0.0-3) unstable; urgency=medium

  * Move to DH_COMPAT=10
  * Depend on python*-dev not python*-all-dev. Closes: #863994

 -- Alastair McKinstry <mckinstry@debian.org>  Thu, 18 May 2017 07:06:22 +0100

pyferret (7.0.0-2) unstable; urgency=medium

  * B-D on jre-default-headless

 -- Alastair McKinstry <mckinstry@debian.org>  Wed, 31 Aug 2016 23:17:30 +0100

pyferret (7.0.0-1) experimental; urgency=medium

  * New upstream release
  * Experimental python3 build	
  * Include pyferret executable in python*-ferret, with alternatives

 -- Alastair McKinstry <mckinstry@debian.org>  Sun, 14 Aug 2016 10:13:52 +0100

pyferret (1.2.0-2) unstable; urgency=medium

  * Standards-Version: 3.9.8. No changes required
  * Hard-code build-date, platform for bit-reprodducible builds

 -- Alastair McKinstry <mckinstry@debian.org>  Thu, 07 Jul 2016 10:44:53 +0100

pyferret (1.2.0-1) unstable; urgency=medium

  * New upstream release.
  * Fix to copyright; "Expat" license not BSD-3-Clause
  * Standards-Version: 3.9.7. No changes required.

 -- Alastair McKinstry <mckinstry@debian.org>  Tue, 01 Mar 2016 14:51:11 +0000

pyferret (1.1.0-1) unstable; urgency=medium

  * Initial release. (Closes: #732290)
  * Resubmitted with corrections to debian/copyright as recommended by 
    Paul Tagliamonte

 -- Alastair McKinstry <mckinstry@debian.org>  Tue, 01 Mar 2016 10:00:37 +0000
